hsWidget 
========
[![npm version](https://badge.fury.io/js/hswidget.svg)](https://badge.fury.io/js/hswidget) 
[![Build status](https://ci.appveyor.com/api/projects/status/vi1ykc6pd2yo7mb3?svg=true)](https://ci.appveyor.com/project/HelpfulScripts/hswidget)
[![Built with Grunt](https://cdn.gruntjs.com/builtwith.svg)](https://gruntjs.com/) 
[![NPM License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://www.npmjs.com/package/hswidget)

Helpful Scripts UI widgets.

**hsWidget** Provides various UI widgets for use with mithril:

| Widget | Description |
|========|=============|
| &nbsp; {@link Menu.Menu Menu} | A group of horizontal menu items that can trigger actions |
| &nbsp; {@link Button.Button Button} | A simple button widget |
| &nbsp; {@link Collapsible Collapsible} | A panel that will expand znd collapse when the title is clicked |
| &nbsp; {@link Modal Modal} | A modal panel that will cover the entire window until released. |
| &nbsp; {@link AddRemove AddButton} | An inline `+` button that will open a form for adding new elements. |
| &nbsp; {@link AddRemove RemoveButton} | An inline `-` button that will remove an item. |
| &nbsp; {@link TypeAhead TypeAhead} | A TypeAhead search input form. |

## Installation
`npm i hswidget`

